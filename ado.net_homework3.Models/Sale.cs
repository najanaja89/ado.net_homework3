﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework3.Models
{
    public class Sale
    {
        public string DealId { get; set; }
        public string SellerId { get; set; }
        public string SumOfDeal { get; set; }
        public DateTime DateOfDeal = new DateTime();
    }
}
