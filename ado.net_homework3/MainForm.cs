﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ado.net_homework3
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Customers")
            {
                CustomersForm form = new CustomersForm();
                form.Show();
            }
            if (comboBox1.Text == "Sellers")
            {
                SellersForm form = new SellersForm();
                form.Show();
            }
            if (comboBox1.Text == "Sales")
            {
                SalesForm form = new SalesForm();
                form.Show();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
