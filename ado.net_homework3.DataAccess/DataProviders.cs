﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ado.net_homework3.Models;

namespace ado.net_homework3.DataAccess
{
    public class DataProviders
    {
        private readonly string _connectionString;
        private readonly string _providerName;
        private readonly DbProviderFactory _providerFactory;

        public DataProviders()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            _providerName = ConfigurationManager.ConnectionStrings["ConnectionString"].ProviderName;
            _providerFactory = DbProviderFactories.GetFactory(_providerName);

        }


        public List<Customer> GetAllCustomers()
        {
            var dataCustomers = new List<Customer>();

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    command.CommandText = "select * from Customers";

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        var id = dataReader["Id"].ToString();
                        var firstName = dataReader["FirstName"].ToString();
                        var lastName = dataReader["LastName"].ToString();

                        dataCustomers.Add(new Customer
                        {
                            Id = id,
                            FirstName = firstName,
                            LastName = lastName

                        });
                    }

                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
            return dataCustomers;
        }

        public List<Seller> GetAllSellers()
        {
            var dataSellers = new List<Seller>();

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    command.CommandText = "select * from Sellers";

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        var id = dataReader["Id"].ToString();
                        var firstName = dataReader["FirstName"].ToString();
                        var lastName = dataReader["LastName"].ToString();

                        dataSellers.Add(new Seller
                        {
                            Id = id,
                            FirstName = firstName,
                            LastName = lastName

                        });
                    }

                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
            return dataSellers;
        }


    }
}
